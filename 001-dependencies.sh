#!/bin/bash

eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

# 3.5.10
pyenv activate v5

echo "Using $(python --version)"
echo "Install pip==20.3.4"
pip install --force-reinstall -v "pip==20.3.4"

echo "Installing dependencies..."
pip install -r AI/requirements-cpu.txt

# 3.6.15
pyenv activate v6

echo "Using $(python --version)"
echo "Installing dependencies..."
pip install -r Py/requirements-cpu.txt

# disable pyenv
pyenv deactivate
