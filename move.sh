#!/bin/bash

# $1 drive directory
# $2 output directory

echo "Copying $1 to $2"
cp $(readlink -f $1)/* $2/ 
echo "DONE"
