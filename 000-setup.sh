#!/bin/bash

# DEPENDENCIES
# - pyenv
# - pyenv-virtualenv
# - tkinter

eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

# install python5 with tkinter

env PYTHON_CONFIGURE_OPTS="--enable-shared" pyenv install 3.5.10
pyenv install 3.6.15

pyenv virtualenv 3.5.10 v5
pyenv virtualenv 3.6.15 v6
