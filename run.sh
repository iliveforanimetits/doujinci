#!/bin/bash

DECENDIR="$HOME/.local/doujinci/Py/decensor_output"

eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

if [ $# -lt 3 ]; then
    echo "Please provide options."
    echo "USAGE:   run.sh LINKORID BARORMOSAIC STREMOVE"
    echo "EXAMPLE: run.sh 123456   bar         true"
    exit 1
fi

export LINKORID=$1
export BARORMOSAIC=$2
export STREMOVE=$3

# 3.5.10
pyenv activate v5

echo "Using $(python --version)"

echo "Running HentAI"
python AI/main.py $LINKORID $BARORMOSAIC $STREMOVE

# 3.6.15
pyenv activate v6

echo "Using $(python --version)"
echo "Running DeepCreamPy"
python Py/decensor.py

# disable pyenv
pyenv deactivate

# move decensored images to original directory
# SANDIR=$(basename "$LINKORID" | sed -r 's/[\/\ \\\.]+/-/g')
mkdir -p "$LINKORID/decensor"
./move.sh "$DECENDIR" "$LINKORID/decensor"

# Send notification with SSH

notify-send -u critical -t 0 "Finished Processing 🏁"

# replace user@hostname with remote machine
# DISPLAY=:0.0 should be replaced with $DISPLAY variable of host machine
# ssh user@hostname 'export DISPLAY=:0.0 && notify-send -u critical -t 0 "REMOTE: Finished Processing 🏁"'
